package io.github.thang86.themovie.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import io.github.thang86.themovie.view.activity.MainActivity


/**
 *
 * Created by Thang86
 */
abstract class BaseFragment : Fragment(), BaseView {
    private var context: MainActivity? = null

    override fun setErrorParent(data: Any) {
        AlertDialog.Builder(requireContext())
            .setTitle("Loi roi ban eii")
            .setMessage(data.toString())
            .setCancelable(false)
            .setPositiveButton("ok") { _, _ ->

            }.show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return setView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewCreated(view, savedInstanceState)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context = context as MainActivity?
    }

    protected abstract fun setView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    protected abstract fun viewCreated(view: View, savedInstanceState: Bundle?)
    protected fun mContext(): MainActivity? {
        return this.context
    }

}
