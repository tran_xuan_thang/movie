package io.github.thang86.themovie.data.local.model

/**
 *
 * Created by Thang86
 */
data class Dates(var maximum:String ="",var minimum:String="")
