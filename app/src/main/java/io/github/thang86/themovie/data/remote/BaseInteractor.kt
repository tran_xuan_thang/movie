package io.github.thang86.themovie.data.remote

/**
 *
 * Created by Thang86
 */
abstract class BaseInteractor {

    protected abstract fun callAPi(): CallApi

}
